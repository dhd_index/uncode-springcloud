package cn.uncode.springcloud.admin;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import cn.uncode.springcloud.admin.common.AdminResultCodeNO;
import cn.uncode.springcloud.starter.boot.app.AppInfo;
import cn.uncode.springcloud.starter.boot.app.UncodeApplication;
import cn.uncode.springcloud.starter.web.result.R;
import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * @author juny
 */
@ServletComponentScan(basePackages= {"cn.uncode.cache"})
@EnableDiscoveryClient
@SpringBootApplication
@EnableAdminServer
@ComponentScan({"cn.uncode.springcloud","cn.uncode"})
public class AdminServerApplication implements WebMvcConfigurer{
	
	@LoadBalanced
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	

    public static void main(String[] args) {
    	UncodeApplication.run(AppInfo.APPLICATION_ADMIN, AdminServerApplication.class, args);
    	R.setCodeKeys(AdminResultCodeNO.RESULT_CODE_KEY, AdminResultCodeNO.RESULT_MESSAGE_KEY, 
				AdminResultCodeNO.RESULT_DATA_KEY, AdminResultCodeNO.RESULT_SUCCESS_KEY);
    	R.setDefaultSuccessCode(AdminResultCodeNO.MSG_CODE_SUCCESS.getCode());
    	R.setDefaultFailureCode(AdminResultCodeNO.MSG_CODE_FAILURE.getCode());
		
    }
    
}
