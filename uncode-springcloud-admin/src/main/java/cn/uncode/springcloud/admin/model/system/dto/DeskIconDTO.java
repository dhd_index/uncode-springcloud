package cn.uncode.springcloud.admin.model.system.dto;

import java.util.Date;

import cn.uncode.dal.annotation.Field;
import cn.uncode.dal.annotation.Table;
import lombok.Data;
/**
 * 数据库实体类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-05-15
 */
@Data
@Table(name = "desk_icon")
public class DeskIconDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final static String TABLE_NAME = "desk_icon";
	public final static String ID = "id";
	public final static String NAME = "name";
	public final static String URL = "url";
	public final static String OPEN_TYPE = "open_type";
	public final static String TITLE = "title";
	public final static String ICON = "icon";
	public final static String STATUS = "status";
	public final static String CREATE_TIME = "create_time";
	public final static String MENU_TYPE = "menu_type";
	public static final String PID = "pid";
	public final static String ORDER = "order";

	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 桌面图标名字
	 */
	private String name;
	/**
	 * 图标打开窗口的请求地址
	 */
	private String url;
	/**
	 * 窗口类型
	 */
	 @Field(name = "open_type")
	private Integer openType;
	/**
	 * 窗口标题
	 */
	private String title;
	/**
	 * 桌面图标路径
	 */
	private String icon;
	/**
	 * 状态（1:在用，2：禁用，3：待审核）
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	 @Field(name = "create_time")
	private Date createTime;
	/**
	 * 菜单类型
	 */
	 @Field(name = "menu_type")
	private Integer menuType;
	 
	/** 主键id*/
	 private Long pid;
	 
	 private int order;


}