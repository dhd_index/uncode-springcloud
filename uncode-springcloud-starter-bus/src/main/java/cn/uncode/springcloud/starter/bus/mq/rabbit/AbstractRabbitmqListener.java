package cn.uncode.springcloud.starter.bus.mq.rabbit;



import java.io.IOException;

import org.springframework.amqp.core.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.amqp.support.AmqpHeaders;

import com.rabbitmq.client.Channel;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public abstract class AbstractRabbitmqListener {
	
	 public void process(@Payload MessageWithTime message, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag, Channel channel)throws Exception {
	        
	        channel.basicAck(deliveryTag, false);
	 }
	 
	 public abstract boolean handleMessage(Object message);
 
 
	 /**
	  * 成功
	  * @param message
	  * @param channel
	  */
 	protected void basicAck(long deliveryTag, Channel channel) {
 		try {
			channel.basicAck(deliveryTag, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
 	}
 	
 	/**
 	 * 失败
 	 * @param message
 	 * @param channel
 	 */
 	protected void basicNack(long deliveryTag, Channel channel) {
 		try {
 			 //避免过多失败log
            Thread.sleep(Constants.ONE_SECOND);
 			channel.basicNack(deliveryTag, false, false);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
 	}

}
